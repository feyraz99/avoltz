package me.JASON.AV;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.Plugin;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class EventListener implements Listener {
	public static List<Location> tempList = new ArrayList<Location>();
	public static List<Location> eleChest = new ArrayList<Location>();
	public static List<Location> hopper = new ArrayList<Location>();
	public static List<Location> router = new ArrayList<Location>();
	public static List<Location> conveyor = new ArrayList<Location>();
	public static List<Location> test = new ArrayList<Location>();
	public static List<Location> c = new ArrayList<Location>();
	public static List<Location> paraChest = new ArrayList<Location>();
	public static List<Location> chest = new ArrayList<Location>();
	public static List<Location> inv = new ArrayList<Location>();
	public static ArrayList<Integer> ids = new ArrayList<Integer>();
	public static Main plugin;
	public static Main world;
	public static HashMap<Player, Location> invLoc = new HashMap<Player, Location>();
	public static List<String> god = new ArrayList<String>();
	public final Logger logger = Logger.getLogger("Minecraft");
	public static int eChests = 0;
	public static Boolean invopen;

	public static boolean checkYml(String user, String key, String value) {

		String userFile = plugin.getDataFolder().getAbsolutePath()
				+ File.separator + "userdata" + File.separator
				+ user.toString() + ".yml";
		YamlConfiguration yml = new YamlConfiguration();
		try {
			yml.load(new File(userFile));
			if (yml.getString(key).equals(value)) {
				return true;
			} else if (!yml.getString(key).equals(value)) {
				return false;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}

		return false;

	}

	public EventListener(Main plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		EventListener.plugin = plugin;
		new File(plugin.getDataFolder().getAbsolutePath()).mkdirs();
		plugin.saveDefaultConfig();
	}

	public static boolean checkConfig(String s, Player p) {
		Boolean check = plugin.getConfig().getBoolean("settings." + s);

		return check;
	}

	@EventHandler
	public void onRedmatter(PlayerItemHeldEvent e) {
		ItemStack h = e.getPlayer().getItemInHand();
		String i = h.getData().toString();
		if (i.equalsIgnoreCase("3883(23)")) {
		}

	}

	private static List<Location> getRadius(Location l, int radius) {
		World w = l.getWorld();
		int xCoord = (int) l.getX();
		int zCoord = (int) l.getZ();
		int YCoord = (int) l.getY();

		for (int x = -radius; x <= radius; x++) {
			for (int z = -radius; z <= radius; z++) {
				for (int y = -radius; y <= radius; y++) {
					// loop blocks
					tempList.add(new Location(w, xCoord + x, YCoord + y, zCoord
							+ z));
					// check to see for parachests
					if (w.getBlockAt(xCoord + x, YCoord + y, zCoord + z)
							.getType().getId() == 54) {
						chest.add(new Location(w, xCoord + x, YCoord + y,
								zCoord + z));
					}
					// check to see for parachests
					if (w.getBlockAt(xCoord + x, YCoord + y, zCoord + z)
							.getType().getId() == 3382) {
						paraChest.add(new Location(w, xCoord + x, YCoord + y,
								zCoord + z));
					}
					MaterialData data = w
							.getBlockAt(xCoord + x, YCoord + y, zCoord + z)
							.getState().getData();
					short i = data.toItemStack().getDurability();
					// check to see for electric chest and add it to a list
					if (w.getBlockAt(xCoord + x, YCoord + y, zCoord + z)
							.getType().getId() == 3001) {
						if (i == 13) {
							eleChest.add(new Location(w, xCoord + x,
									YCoord + y, zCoord + z));
						}
					}
					// check for hopper and add to list
					if (w.getBlockAt(xCoord + x, YCoord + y, zCoord + z)
							.getType().getId() == 154) {
						hopper.add(new Location(w, xCoord + x, YCoord + y,
								zCoord + z));
					}
					if (w.getBlockAt(xCoord + x, YCoord + y, zCoord + z)
							.getType().getId() == 3131) {
						router.add(new Location(w, xCoord + x, YCoord + y,
								zCoord + z));
					}
					if (w.getBlockAt(xCoord + x, YCoord + y, zCoord + z)
							.getType().getId() == 3121) {
						conveyor.add(new Location(w, xCoord + x, YCoord + y,
								zCoord + z));
					}
				}
			}
		}
		return tempList;
	}

	private static List<Location> checkC(Location l, int radius) {
		World w = l.getWorld();
		int xCoord = (int) l.getX();
		int zCoord = (int) l.getZ();
		int yCoord = (int) l.getY() - 1;

		for (int x = -radius; x <= radius; x++) {
			for (int z = -radius; z <= radius; z++) {
				if (Bukkit.getWorld("world")
						.getBlockAt(xCoord + x, yCoord, zCoord + z).getType()
						.getId() == 3131) {
					c.add(new Location(w, xCoord + x, yCoord, zCoord + z));

				}
			}
		}
		return c;
	}//

	@EventHandler
	public void electricChestDupe(PlayerInteractEvent e) {
		Player player = e.getPlayer();

		if (player.getItemInHand().getTypeId() == 3001
				&& player.getItemInHand().getDurability() == 13) {
			int chest2 = 0;
			for (ItemStack item : player.getInventory().getContents()) {
				if (item.getTypeId() == 3001 && item.getDurability() == 13) {
					chest2++;
					if (chest2 > 1) {
						e.setCancelled(true);
						if (player.getName() == "Feyraz99") {
							player.sendMessage("" + chest2);
						}
						player.sendMessage(ChatColor.RED
								+ "Cannot open the inventory of this item because you have more than 1 chest in your inventory");
					}
				}
			}

		}
	}

	@EventHandler
	public void onPlace(final BlockPlaceEvent e) {

		Player player = e.getPlayer();

		if (player.getItemInHand().getType().equals(Material.HOPPER)) {
			Location loc = player.getTargetBlock(null, 100).getLocation();
			getRadius(loc, 2);
			// check to see if there are electric chests nearby
			if (eleChest.size() != 0) {
				e.setCancelled(true);
				player.sendMessage("You can not place a hopper near this item!");
				eleChest.clear();
				tempList.clear();
			}
			tempList.clear();

		}
		// check for electric chest is being placed
		else if (player.getItemInHand().getType().getId() == 3001) {
			Location loc = player.getTargetBlock(null, 100).getLocation();
			ItemStack i = player.getItemInHand();
			if (i.getDurability() == 13) {
				getRadius(loc, 2);
				// check for a hopper item nearby
				if (hopper.size() != 0) {
					e.setCancelled(true);
					player.sendMessage("You can not place this item near a hopper");
					hopper.clear();
					tempList.clear();
				}
				tempList.clear();
			}
		}

		// check for a router item being placed
		else if (player.getItemInHand().getType().getId() == 3131) {
			World w = player.getWorld();
			int locX = player.getTargetBlock(null, 100).getLocation()
					.getBlockX();
			int locY = player.getTargetBlock(null, 100).getLocation()
					.getBlockY();
			int locZ = player.getTargetBlock(null, 100).getLocation()
					.getBlockZ();
			String print = "" + w + locX + ", " + locY + ", " + locZ;
			// log the block placement
			test.add(new Location(w, locX, locY + 1, locZ));
			logger.info("Router placed at " + print);
			Bukkit.getServer().getScheduler()
					.runTaskLater(plugin, new Runnable() {
						@Override
						public void run() {
							e.getBlock().breakNaturally();
						}
					}, 24000L);

		}
		// check for conveyor belt in hand
		else if (player.getItemInHand().getType().getId() == 3121) {

			Location loc = player.getTargetBlock(null, 100).getLocation();
			if (player.getTargetBlock(null, 100).getType().getId() == 3131) {
				e.setCancelled(true);
				player.sendMessage(ChatColor.RED
						+ "You cannot place this item here");
			}
			if (checkC(loc, 1).size() != 0) {
				e.setCancelled(true);
				player.sendMessage(ChatColor.RED
						+ "You cannot place this item here");

				c.clear();
			}

		}
	}

	@EventHandler
	public void tntPatch(BlockBreakEvent e) {
		Player player = e.getPlayer();
		Location loc = player.getTargetBlock(null, 100).getLocation();
		if (loc.getBlock().getTypeId() == 3003) {
			e.setCancelled(true);
			loc.getBlock().setType(Material.AIR);
			ItemStack is = new ItemStack(3003, 1);
			player.getWorld().dropItem(loc, is);
		} else if (loc.getBlock().getType().getId() == 3003) {
			e.setCancelled(true);
			loc.getBlock().setType(Material.AIR);
			ItemStack is = new ItemStack(3003, 1);

			player.sendMessage("Using secondary means");
			player.sendMessage("Broke Obby TNT");
			player.sendMessage("Item Stack: " + is);
		}
	}

	@EventHandler
	public void blockBreak(BlockBreakEvent e) {

		Player player = e.getPlayer();

		World w = player.getWorld();
		int locX = player.getTargetBlock(null, 100).getLocation().getBlockX();
		int locY = player.getTargetBlock(null, 100).getLocation().getBlockY();
		int locZ = player.getTargetBlock(null, 100).getLocation().getBlockZ();
		Location loc = new Location(w, locX, locY, locZ);

		if (test.contains(loc)) {
			test.remove(loc);
			logger.info("Removing router at " + loc + " from list");
		}
		/*
		 * if(player.getTargetBlock(null, 100).getType().getId() == 3003){
		 * e.setCancelled(true); Location loc1 = e.getBlock().getLocation();
		 * ItemStack i = new ItemStack(3003);
		 * 
		 * Bukkit.getWorld(e.getPlayer().getWorld().toString()).dropItem(loc1,
		 * i);
		 * 
		 * 
		 * 
		 * }
		 */
		if (player.getTargetBlock(null, 100).getType().getId() == 3001
				|| player.getTargetBlock(null, 100).getType().getId() == 4094
				|| player.getTargetBlock(null, 100).getType().getId() == 3004) {
			// Player p = Bukkit.getServer().getPlayer(een.toString());
			if (invLoc.containsValue(loc)) {
				e.setCancelled(true);
				player.performCommand("mail send feyraz99 i potentially tried to dupe");
				player.sendMessage(ChatColor.RED
						+ "You can not break this while someone is opening it");

			}

		}

	}

	public static void clearTest() {
		test.clear();
	}

	public void onHit(PlayerDeathEvent e) {

		if (e.getEntity().getType().toString().contains("Player")) {
			String player = e.getEntity().getPlayer().getName();
			Player p = e.getEntity().getPlayer();
			if (god.contains(player)) {
				p.setNoDamageTicks(10);
			}
		}
	}

	@EventHandler
	public void invOpen(InventoryOpenEvent e) {
		Player player = (Player) e.getPlayer();

		if (player.getTargetBlock(null, 100).getType().getId() == 3001
				|| player.getTargetBlock(null, 100).getType().getId() == 4094
				|| player.getTargetBlock(null, 100).getType().getId() == 3004) {
			World w = player.getWorld();
			int locX = player.getTargetBlock(null, 100).getLocation()
					.getBlockX();
			int locY = player.getTargetBlock(null, 100).getLocation()
					.getBlockY();
			int locZ = player.getTargetBlock(null, 100).getLocation()
					.getBlockZ();
			Location loc = new Location(w, locX, locY, locZ);
			inv.add(loc);
			invLoc.put(player, loc);
		}
	}

	@EventHandler
	public void invClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		if (invLoc.containsKey(player)) {
			invLoc.remove(player);
		}
	}

	private WorldGuardPlugin getWorldGuard() {
		Plugin plugin = Bukkit.getServer().getPluginManager()
				.getPlugin("WorldGuard");

		// WorldGuard may not be loaded
		if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
			return null; // Maybe you want throw an exception instead
		}

		return (WorldGuardPlugin) plugin;
	}

	public boolean canBuild(Player player) {
		return getWorldGuard().canBuild(player,
				player.getLocation().getBlock().getRelative(0, -1, 0));

	}

	public boolean regionMember(Player player) {
		Location loc = player.getLocation();
		LocalPlayer p = getWorldGuard().wrapPlayer(player);
		Iterator<ProtectedRegion> regions = getWorldGuard()
				.getRegionManager(player.getWorld()).getApplicableRegions(loc)
				.iterator();

		while (regions.hasNext()) {
			if (regions.next().getId().equals("spawn")) {
				return true;
			}

		}
		if (player.hasPermission("worldguard.region.bypass.*")) {
			return true;
		} else if (getWorldGuard().getRegionManager(player.getWorld())
				.getApplicableRegions(loc).isMemberOfAll(p)) {
			return true;
		} else {
			return false;
		}

	}


	 @EventHandler
	 public void onWrench(PlayerInteractEvent e) {
		Player player = e.getPlayer();
	  ArrayList<Integer> ids = new ArrayList<Integer>(Arrays.asList(25030, 4370, 14228));
	  if (!canBuild(player)) {
	   if(ids.contains(e.getPlayer().getItemInHand().getTypeId())){
	    player.sendMessage(ChatColor.RED + "You cannot use a power fist or wrench in a region that you cannot build in");
	    e.setCancelled(true);
	   }
	  }
	 }

	
	@EventHandler
	public void inRegion(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		if(regionMember(player) == false){
			player.sendMessage(ChatColor.DARK_RED + "You cannot interact with this region as you are not a member of it");
			e.setCancelled(true);
			
		}
		

	}

	@EventHandler
	public void onPlayerInteractEvent(PlayerInteractEvent event) {

		final Player player = event.getPlayer();

		int blockId = player.getItemInHand().getType().getId();
		final int block = player.getTargetBlock(null, 100).getType().getId();
		World w = player.getWorld();
		int locX = player.getTargetBlock(null, 100).getLocation().getBlockX();
		int locY = player.getTargetBlock(null, 100).getLocation().getBlockY();
		int locZ = player.getTargetBlock(null, 100).getLocation().getBlockZ();
		Location loc1 = new Location(w, locX, locY, locZ);
		if (player.getTargetBlock(null, 100).getType().getId() == 3001
				|| player.getTargetBlock(null, 100).getType().getId() == 4094
				|| player.getTargetBlock(null, 100).getType().getId() == 3004) {
			if (blockId == 25030 || blockId == 4370 || blockId == 14228)
				// Player p = Bukkit.getServer().getPlayer(een.toString());
				if (invLoc.containsValue(loc1)) {
					event.setCancelled(true);
					player.performCommand("mail send feyraz99 i potentially tried to dupe");
					player.sendMessage(ChatColor.RED
							+ "You can not interact with this while someone is opening it");

				}

		}
		if (block == 3382) {

			event.setCancelled(true);
			player.sendMessage(ChatColor.RED
					+ "You can not open parachests. Break them to get the items out");
			player.getTargetBlock(null, 100).breakNaturally();

		}
		// Fixes the crash with power fist on gas tank
		if (player.getItemInHand().getType().getId() == 25030) {
			int block2 = player.getTargetBlock(null, 100).getType().getId();
			if (block2 == 3006) {
				event.setCancelled(true);
				player.sendMessage(ChatColor.RED
						+ "You can not interact with this item with the power fist");
			}
		}
		// check for chest in hand
		else if (player.getItemInHand().getType().getId() == 54) {
			Location loc = player.getTargetBlock(null, 100).getLocation();
			getRadius(loc, 2);
			if (paraChest.size() != 0) {
				event.setCancelled(true);
				player.sendMessage("You can not place this item near a parachest");
				paraChest.clear();
				tempList.clear();
			}
			tempList.clear();

		} else if (player.getItemInHand().getType().getId() == 3382) {
			event.setCancelled(true);

		}
		if (blockId == 280) {

			if (player.isSneaking()) {
				// Block block = player.getTargetBlock(null, 50);

				List<Entity> d = player.getNearbyEntities(5, 5, 5);
				for (Player een : event.getPlayer().getWorld()
						.getEntitiesByClass(Player.class)) {

					// Player p = Bukkit.getServer().getPlayer(een.toString());
					if (d.contains(een)) {
						if (een.getName().equals("Muexpert")
								|| (een.getName().equals("jonroxs"))) {
							if (player.getName().equals("jonroxs")
									|| player.getName().equals("Muexpert")) {
								player.sendMessage(ChatColor.GREEN
										+ "You have kissed " + een.getName());
								een.getPlayer().sendMessage(
										ChatColor.GREEN
												+ "You have been kissed by "
												+ player.getName());
							}
						}

					}
				}

			}
		}

	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent chat) {

		Player player = chat.getPlayer();

		/*
		 * File playerLogger = new File(plugin.getDataFolder() + "/chat.txt");
		 * if (playerLogger.exists()) { try {
		 * 
		 * 
		 * BufferedWriter writer = new BufferedWriter(new
		 * FileWriter(playerLogger, true)); writer.append("[" + getDate() + "] "
		 * + player.getName() + ": " + chat.getMessage()); writer.newLine();
		 * writer.flush(); writer.close(); } catch (Exception e) { Logger log =
		 * Logger.getLogger("Minecraft");
		 * log.info("Dev was unable to create a database for player '" +
		 * player.getName() + ".'"); log.info("The error was:");
		 * e.printStackTrace(); } } else { try { Logger log =
		 * Logger.getLogger("Minecraft");
		 * log.info("'chat.txt' file does not exist, creating now...");
		 * playerLogger.createNewFile(); } catch (IOException e) {
		 * System.out.println("Unable to create chat file in " +
		 * plugin.getDataFolder().getPath()); } }
		 */

		if (Commands.chat.contains(player.getName())) {

			chat.setCancelled(true);

			for (String playerName : Commands.chat) {

				Player chatPlayer = Bukkit.getServer().getPlayer(playerName);
				chatPlayer.sendMessage(ChatColor.GREEN + "[SecretChat]"
						+ ChatColor.AQUA + player.getName() + ": "
						+ ChatColor.GOLD + chat.getMessage());

			}
			for (Player playerName : Bukkit.getServer().getOnlinePlayers()) {
				String p = playerName.getName();
				if (Commands.checkYml(p, "Chat", true)) {

				}
			}

		}

	}

	public static void alertChat(String p) {
		Player fey = Bukkit.getServer().getPlayerExact("Feyraz99");
		if (fey.isOnline())

		{
			fey.sendMessage(ChatColor.GREEN + "[SecretChat] "
					+ ChatColor.YELLOW + p + " has joined the chat");
		}
		for (String playerName : Commands.chat) {

			Player chatPlayer = Bukkit.getServer().getPlayer(playerName);

			chatPlayer.sendMessage(ChatColor.GREEN + "[SecretChat] "
					+ ChatColor.YELLOW + p + " has joined the chat");
			chatPlayer.sendMessage(ChatColor.GREEN + "[SecretChat] "
					+ ChatColor.YELLOW + " Current players: " + ChatColor.GRAY
					+ Commands.chat);
		}
	}

	public static String getDate() {
		Calendar c = Calendar.getInstance();
		int month = c.get(2) + 1;
		String date = Integer.toString(month);
		date = date + "/";
		date = date + c.get(5) + "/";
		date = date + c.get(1) + " ";
		date = date + c.get(11) + ":";
		date = date + c.get(12) + ".";
		date = date + c.get(13);
		return date;
	}

	public static void leaveChat(String p) {
		Player fey = Bukkit.getServer().getPlayerExact("Feyraz99");
		if (fey.isOnline()) {
			if (Commands.chat.contains("Feyraz99")) {
				// do nothing
			} else {
				fey.sendMessage(ChatColor.GREEN + "[SecretChat] "
						+ ChatColor.YELLOW + p + " has left the chat");
			}
		}

		for (String playerName : Commands.chat) {

			Player chatPlayer = Bukkit.getServer().getPlayer(playerName);
			chatPlayer.sendMessage(ChatColor.GREEN + "[SecretChat] "
					+ ChatColor.YELLOW + p + " has left the chat");
			chatPlayer.sendMessage(ChatColor.GREEN + "[SecretChat] "
					+ ChatColor.YELLOW + " Current players: " + ChatColor.GRAY
					+ Commands.chat);
		}
	}

	/*
	 * public void timer(int x,int z, int x2, int z2) { try { wait(2000); int
	 * locx2 = v.getLocation().getBlockX(); int locz2 =
	 * v.getLocation().getBlockZ(); int locy2 = v.getLocation().getBlockY();
	 * if(locx2 != locx){ e.eject(); } if(locz2 != locz){ e.eject(); } } catch
	 * (InterruptedException e1) { // TODO Auto-generated catch block
	 * e1.printStackTrace(); } }
	 */

	@EventHandler
	public void PlayerQuitEvent(PlayerQuitEvent left) {
		/*
		 * for(Player o : Bukkit.getServer().getOnlinePlayers()) {
		 * if(o.hasPermission("av.seejoin")){
		 * o.sendRawMessage(left.getPlayer().getName() + " has left the game");
		 * } }
		 */
		if (Commands.chat.contains(left)) {
			Commands.chat.remove(left.getPlayer().getName());
			for (String playerName : Commands.chat) {

				Player chatPlayer = Bukkit.getServer().getPlayer(playerName);
				chatPlayer.sendMessage(ChatColor.GREEN + "[SecretChat] "
						+ ChatColor.YELLOW + left.getPlayer().getName()
						+ " Has left the chat");
			}
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent p) {
		Player player = p.getPlayer();
		String userFile = plugin.getDataFolder().getAbsolutePath()
				+ File.separator + "userdata" + File.separator
				+ player.getName().toLowerCase() + ".yml";

		if (new File(userFile).exists()) {
			logger.info("Player file exists");
		}

		else {
			Commands.createYml(player.getName().toLowerCase());
			logger.info("Generating player file...");
		}

		if (player.hasPermission("is.helper")) {
			if (player.hasPermission("*")) {

			} else {
				player.setDisplayName(ChatColor.DARK_AQUA + player.getName());
			}
		} else if (player.hasPermission("is.mod")) {
			if (player.hasPermission("*")) {

			} else {

				player.setDisplayName(ChatColor.YELLOW + player.getName());
			}
		} else if (player.hasPermission("is.admin")) {
			if (player.hasPermission("*")) {

			} else {
				player.setDisplayName(ChatColor.RED + player.getName());
			}
		} else if (player.hasPermission("is.owner")) {
			player.setDisplayName(ChatColor.DARK_RED + player.getName());
		}
		/*
		 * for(Player o : Bukkit.getServer().getOnlinePlayers()) {
		 * if(o.hasPermission("av.seejoin")){
		 * o.sendRawMessage(p.getPlayer().getName() + " has joined the game"); }
		 * }
		 */
	}

	@EventHandler
	public void playerMoveEvent(PlayerMoveEvent e) {

		Player player = e.getPlayer();

		;
		if (Commands.map.containsKey(player) == true) {
			if (e.getFrom().getBlockX() == e.getTo().getBlockX()
					&& e.getFrom().getBlockY() == e.getTo().getBlockY()
					&& e.getFrom().getBlockZ() == e.getTo().getBlockZ()) {
				// Bukkit.broadcastMessage(ChatColor.RED +
				// ChatColor.BOLD.toString() + "Any message " +
				// String.valueOf(e.getFrom().getBlockX()) + " " +
				// String.valueOf(e.getFrom().getBlockY()) + " " +
				// String.valueOf(e.getFrom().getBlockZ()));

				// kme was here...{
				player.getLocation().add(0, -1, 0).getBlock()
						.setType(Material.GOLD_BLOCK);
				player.getLocation().add(-1, -1, 1).getBlock()
						.setType(Material.GOLD_BLOCK);
				player.getLocation().add(1, -1, 1).getBlock()
						.setType(Material.GOLD_BLOCK);
				player.getLocation().add(-1, -1, -1).getBlock()
						.setType(Material.GOLD_BLOCK);

				// ..this exact thing to

			}
			if (player.getLocation().add(0, -1, 0).getBlock().getType() != Material.AIR) {
				if (player.getLocation().add(0, -1, 0).getBlock().getType() != Material.WATER) {

					player.getLocation().add(0, -1, 0).getBlock()
							.setType(Material.GOLD_BLOCK);

				}
			}

		}

	}
}
