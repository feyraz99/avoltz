package me.JASON.AV;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class Main extends JavaPlugin {

	public final Logger logger = Logger.getLogger("Minecraft");
	public static Main plugin;

	public static WorldGuardPlugin getWorldGuard() {
		Plugin plugin = Bukkit.getServer().getPluginManager()
				.getPlugin("WorldGuard");
		if ((plugin == null) || (!(plugin instanceof WorldGuardPlugin))) {
			return null; // throws a NullPointerException, telling the Admin
							// that WG is not loaded.
		}
		return (WorldGuardPlugin) plugin;
	}

	@Override
	public void onDisable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " has been disabled");
	}

	@Override
	public void onEnable() {
		getWorldGuard();

		this.getCommand("playerloginfo").setExecutor(new Commands(this));
		this.getCommand("checklog").setExecutor(new Commands(this));
		this.getCommand("welcome").setExecutor(new Commands(this));
		this.getCommand("mob").setExecutor(new Commands(this));
		this.getCommand("charge").setExecutor(new Commands(this));
		this.getCommand("rgedit").setExecutor(new Commands(this));
		this.getCommand("clearrouters").setExecutor(new Commands(this));
		this.getCommand("jailall").setExecutor(new Commands(this));
		this.getCommand("muteall").setExecutor(new Commands(this));
		this.getCommand("forums").setExecutor(new Commands(this));
		this.getCommand("vote").setExecutor(new Commands(this));
		this.getCommand("trail").setExecutor(new Commands(this));
		this.getCommand("avadmin").setExecutor(new Commands(this));
		this.getCommand("avoltz").setExecutor(new Commands(this));
		this.getCommand("rtp").setExecutor(new Commands(this));
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " Version " + pdfFile.getVersion()
				+ " has been enabled");
		new EventListener(this);
		BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				for (World w : Bukkit.getWorlds()) {
					for (Entity e : w.getEntities()) {

						if (e.getType().getName().contains("ICBM")) {
							e.remove();
							logger.info("Removed one");
						}

					}
				}
				for (Player p : Bukkit.getServer().getOnlinePlayers()) {
					if (p.hasPermission("essentials.mute")) {
						p.sendMessage(ChatColor.DARK_RED
								+ "["
								+ ChatColor.RED
								+ "Alert-Staff"
								+ ChatColor.DARK_RED
								+ "]"
								+ ChatColor.AQUA
								+ " Staff only receive this message, If you see someone doing something wrong take a screenshot and save it!");

					}
				}

			}
		}, 0L, 6000L);
		

	}
}